#################################
# create package
#################################


# generate plots for manual:
# needs package already installed!!
source("./utility/inst/utility_manual_plots.r")

# check package:
system("R CMD check utility")

# build source package (.tar.gz):
system("R CMD build utility")

# check processed package:
system("R CMD check --as-cran utility_1.4.6.tar.gz")

install.packages("utility_1.4.6.tar.gz",repos=NULL,type="source")
library(utility)
help(utility)

# upload:
# https://cran.r-project.org/submit.html

